package cat.itb.whatsapp.activities;

import android.util.EventLog;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewInteraction;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.whatsapp.R;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressKey;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.core.internal.deps.guava.base.Preconditions.checkNotNull;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.EasyMock2Matchers.equalTo;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@RunWith(AndroidJUnit4.class)
public class CreateNewChat {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule mGrantPermissionRule =
            GrantPermissionRule.grant(
                    "android.permission.READ_CONTACTS");


    //TODO: AUX
    @Test
    public void login() {

        ViewInteraction aceptar_y_continuar = onView(
                allOf(withId(R.id.accept_continue), withText("Aceptar y continuar"),
                        childAtPosition(
                                childAtPosition(withClassName(is("android.widget.ScrollView")), 0),
                                3)));
        aceptar_y_continuar.perform(click());


        ViewInteraction textInputEditText = onView(
                allOf(withId(R.id.phone_number),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        2),
                                1),
                        isDisplayed()));
        textInputEditText.perform(replaceText("623456789"), closeSoftKeyboard());



        ViewInteraction ok = onView(
                allOf(withId(R.id.button_ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                        0),
                                1),
                        isDisplayed()));
        ok.perform(click());



        //Sleep to read the database
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction writeUserName = onView(
                allOf(withId(R.id.user_name),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                        2),
                                0),
                        isDisplayed()));
        writeUserName.perform(replaceText("Jhosmar"), closeSoftKeyboard());


        ViewInteraction ok2 = onView(
                allOf(withId(R.id.button_ok), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                        0),
                                1),
                        isDisplayed()));
        ok2.perform(click());


    }


    @Test
    public void createNewChat(){

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.btn_new_message), withContentDescription("new chat"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatImageButton.perform(click());


      ViewInteraction recyclerView = onView(
                allOf(withId(R.id.rv_contacts),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));

      recyclerView.perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));


        ViewInteraction writeMessage = onView(
                allOf(withId(R.id.edit_text_message),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.constraintlayout.widget.ConstraintLayout")),
                                        3),
                                1),
                        isDisplayed()));
        writeMessage.perform(replaceText("Hi!!")).perform(closeSoftKeyboard());


        //Send Message
        onView(withId(R.id.btn_send)).perform(click());


        //check sent message
        ViewInteraction message = onView(
                allOf(withId(R.id.mensaje_texto), withText("Hi!!"),
                        withParent(allOf(withId(R.id.text_box_message),
                                withParent(IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class)))),
                        isDisplayed()));
        message.check(matches(withText("Hi!!")));



    }



    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }






}
