package cat.itb.whatsapp;

import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.junit.Rule;
import org.junit.Test;

import cat.itb.whatsapp.activities.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class MainActivityTest {

    @Rule
    public ActivityScenarioRule<MainActivity> publicActivityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);


    @Test
    public void goToEstados(){
        onView(withId(R.id.estados)).check(matches(isDisplayed()));
    }

}
