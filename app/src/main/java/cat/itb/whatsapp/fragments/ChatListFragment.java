package cat.itb.whatsapp.fragments;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.adapters.FirestoreChatsAdapter;
import cat.itb.whatsapp.firebase.FirestoreChat;
import cat.itb.whatsapp.models.User;
import cat.itb.whatsapp.utils.ConvertSharePreferencesToUser;
import cat.itb.whatsapp.utils.SwipeToDeleteCallback;


public class ChatListFragment extends Fragment{

    private RecyclerView recyclerView;

    public static User user;
    // 1 FIREBASE//
    //FirebaseChatsAdapter firebaseChatsAdapter;
    // 1 FIRESTORE //
    FirestoreChatsAdapter firestoreChatsAdapter;

    String numberContact;

    public ChatListFragment() {}

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = ConvertSharePreferencesToUser.getSharedPreferencesInfo(this.getActivity());
        // 2 FIREBASE //
        //FirebaseChat.inicializeDatabase(user.getTelefono());
        // 2 FIRESTORE //
        FirestoreChat.inicializeDatabase(user.getTelefono());

        //TODO recoger el bundle para setear el numero de telefono del contacto

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String phoneContact = bundle.getString("key", null);
        } else {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_chats, container, false);

        recyclerView = view.findViewById(R.id.chats_recyclerview);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //3 FIRESTORE//
        firestoreChatsAdapter = new FirestoreChatsAdapter(FirestoreChat.firebaseFirestoreRecyclerOptions, user);
        firestoreChatsAdapter.setContext(getContext());
        recyclerView.setAdapter(firestoreChatsAdapter);

        setUpRecyclerView();

        return view;
    }



    public void deleteChat(String id) {
        FirestoreChat.delete(id);
    }

    private void setUpRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(firestoreChatsAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onStart() {
        super.onStart();
        //chatListAdapterFirebase.startListening();
        firestoreChatsAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        firestoreChatsAdapter.startListening();
        //chatListAdapterFirebase.stopListening();
    }
}

