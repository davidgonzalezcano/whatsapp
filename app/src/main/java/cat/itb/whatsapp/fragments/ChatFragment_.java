package cat.itb.whatsapp.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.models.Chat;


public class ChatFragment_ extends Fragment {

    private ShapeableImageView shapeableImageView;
    private TextView textViewName, textViewLastMessage, textViewTime;
    //private ConstraintLayout constraintLayout;
    private Chat chat;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_chat_falso, container, false);
        /*constraintLayout = v.findViewById(R.id.constraintLayout);
        shapeableImageView = constraintLayout.findViewById(R.id.imageViewShapable);
        textViewName = constraintLayout.findViewById(R.id.textViewName);
        textViewLastMessage = constraintLayout.findViewById(R.id.textViewLastMessage);
        textViewTime = constraintLayout.findViewById(R.id.textViewTime);*/

        shapeableImageView = v.findViewById(R.id.imageViewContact);
        textViewName = v.findViewById(R.id.textViewName);
        textViewLastMessage = v.findViewById(R.id.textViewTipoLlamada);
        textViewTime = v.findViewById(R.id.textViewTimeLlamada);

        if (getArguments() != null) chat = getArguments().getParcelable("chat");
        if (chat != null) {
            Log.i("tag", " onCreateView ChatFragment"+ chat.getChatId());

            textViewName.setText(chat.getName());
            textViewLastMessage.setText(chat.getLastMessage()+ " 4554");
            textViewTime.setText(chat.getLastMessageTime());


            shapeableImageView.setShapeAppearanceModel(shapeableImageView.getShapeAppearanceModel()
                    .toBuilder()
                    .setTopRightCorner(CornerFamily.ROUNDED,100f)
                    .build());

        }
        return  v;
    }


}