package cat.itb.whatsapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.firebase.FirebaseAuthUser;
import cat.itb.whatsapp.fragments.*;
import cat.itb.whatsapp.models.ChatModelFirestore;
import cat.itb.whatsapp.models.User;
import cat.itb.whatsapp.utils.ConvertSharePreferencesToUser;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CAMERARESULT = 201;
    static final int REQUEST_IMAGE_CAPTURE = 100;
    private static final String DEFAULT_PHOTO = "https://firebasestorage.googleapis.com/v0/b/whatsapp-8bb00.appspot.com/o/users%2F20210317_192237.jpg?alt=media&token=c3d98c18-ca73-4732-b098-51f918cc5c51";

    public static FirebaseAuthUser firebaseAuthUser;
    private String userId;

    public static User user; // este no nos vale para pasarselo a ChatFragment

    private String phoneContactNewChat;
    private ImageButton newMessage;

    public static ChatModelFirestore newChatInMain = new ChatModelFirestore();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuthUser = new FirebaseAuthUser();

        newMessage = findViewById(R.id.btn_new_message);

        newMessage.setOnClickListener(v -> sendUserToNewChatActivity());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {


            // si es != null venimos de la agenda
            if (bundle.get("newChatPhoneNumber") != null ){
                phoneContactNewChat = bundle.getString("newChatPhoneNumber");
                Intent intentToOneChat = new Intent(this, MensajesChatActivity.class);
                setNewChatInMain(phoneContactNewChat);
                intentToOneChat.putExtra("CREATE", 1);
                startActivity(intentToOneChat);
            }


            // si es diferente de null venimos de MensajesChatActivity, hemos seleccionado un contacto
            // para iniciar un chat pero hemos salido sin hablar, entonces no tenemos que hacer nada
            if (bundle.get("FLAG_NO_CREATE_CHAT") != null ){
                Log.e("asd", "intento de crear chat no finalizado");
            }

            //Contactos seleccionados para el nuevo chat
            if (bundle.get("Contacts")!= null){
                Log.e("asdasd", "recibimos contacts");

                String [] contacts = bundle.getStringArray("Contacts");
                Log.e("asdasd", Arrays.deepToString(bundle.getStringArray("Contacts")));
                Intent intentToOneChat = new Intent(this, MensajesChatActivity.class);

                setNewGroupChatInMain(contacts);
                intentToOneChat.putExtra("CREATE", 1);
                startActivity(intentToOneChat);

            }

        } else {
            Log.e("asd", "bundle null in main");
        }


        crearMenu();
    }


    // Listener Nav Bar
    private BottomNavigationView.OnNavigationItemSelectedListener navListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.estados:
                            selectedFragment = new EstadosFragment();
                            break;

                        case R.id.llamadas:
                            selectedFragment = new LlamadasFragment();
                            break;

                        case R.id.camara:

                            dispatchTakePictureIntent();
                            selectedFragment = null;
                            break;

                        case R.id.chats:
                            selectedFragment = new ChatListFragment();
                            break;

                        case R.id.configuracion:
                            selectedFragment = new ConfigurationFragment();
                            break;
                    }

                    // Begin Transaction
                    if (selectedFragment != null) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.nav_host_fragment
                                        , selectedFragment).commit();
                    }
                    return true;
                }
            };

    public void crearMenu() {
        BottomNavigationView btnNav = findViewById(R.id.bottom_navigation);
        btnNav.setSelectedItemId(R.id.chats);

        //badges
        btnNav.getOrCreateBadge(R.id.chats).setNumber(4);
        btnNav.setOnNavigationItemSelectedListener(navListener);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public void sendUserToNewChatActivity() {
        Intent intentToNewChatActivity = new Intent(MainActivity.this, NewChatActivity.class);
        startActivity(intentToNewChatActivity);
    }

    // setea un nuevo chat con el numero de telefono recogido de la agenda
    public void setNewChatInMain(String phoneContactNewChat) {
        String cleanPhone = phoneContactNewChat.replaceAll("\\s+","")
                .replaceAll("\\(", "")
                .replaceAll("\\)","")
                .replaceAll("\\-","");
        String completPhone = "+34" + cleanPhone;

        newChatInMain.setTelephones(new ArrayList<String>() {{
            add(user.getTelefono());
            add(completPhone);
        }});

        newChatInMain.setMessageTime(new Date().toString());
        newChatInMain.setMessage("");
        // TODO: pillar la foto del chat
        newChatInMain.setPhoto(DEFAULT_PHOTO);

        newChatInMain.getConuntMessagesHashMap().put(user.getTelefono(), 0);
        newChatInMain.getConuntMessagesHashMap().put(completPhone, 0);

        newChatInMain.getIsReadMessagesHashMap().put(user.getTelefono(), true);
        newChatInMain.getIsReadMessagesHashMap().put(completPhone, true);


    }

    // setea un nuevo chat grupal
    public void setNewGroupChatInMain(String[] contactsNumber) {
        ArrayList<String> contactsList = new ArrayList<>();

        // add list phones in chat
        for (String number: contactsNumber) {
            contactsList.add(cleanTelephone(number));
        }
        contactsList.add(user.getTelefono());

        // add count messages in chat
        for (String number: contactsList) {
            newChatInMain.getConuntMessagesHashMap().put(number, 0);
            newChatInMain.getIsReadMessagesHashMap().put(number, true);
        }
        newChatInMain.setTelephones(contactsList);
        newChatInMain.setMessageTime(new Date().toString());
        newChatInMain.setMessage("");
        newChatInMain.setPhoto(DEFAULT_PHOTO);
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (firebaseAuthUser.getCurrentUser() == null) {
            Intent loginIntent = new Intent(this, WelcomeActivity.class);
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(loginIntent);

            finish();
        } else {
            userId = firebaseAuthUser.getUid();
            user = ConvertSharePreferencesToUser.getSharedPreferencesInfo(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public String cleanTelephone(String tel) {
        String cleanPhone = tel.replaceAll("\\s+","")
                .replaceAll("\\(", "")
                .replaceAll("\\)","")
                .replaceAll("\\-","");
        String completPhone = "+34" + cleanPhone;

        return completPhone;
    }
}