package cat.itb.whatsapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;


import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.utils.NoUnderlineSpan;

public class RegisterActivity extends AppCompatActivity {

    private TextInputEditText phone_number;
    private MaterialTextView textOk;
    private MaterialTextView registerConditions;
    private String text;
    private ProgressDialog loadingBar;
    private NoUnderlineSpan noUnderlineSpan;
    private String complete_phone_number;


    FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Log.e("wwww", "phoneNumber");


        phone_number = findViewById(R.id.phone_number);
        textOk = findViewById(R.id.button_ok);
        registerConditions = findViewById(R.id.registerConditions);

        mAuth = FirebaseAuth.getInstance();

        loadingBar = new ProgressDialog(this);

        setFields();


        textOk.setOnClickListener(v -> createNewAccount(v));

        phone_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                if (validatePhoneNumber(s.toString())) {
                    textOk.setEnabled(true);
                } else {
                    phone_number.setError("Invalid");
                    textOk.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (validatePhoneNumber(s.toString())) {
                    textOk.setEnabled(true);
                } else {
                    phone_number.setError("Invalid");
                    textOk.setEnabled(false);
                }
            }
        });

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {

            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Log.d("PHONE_VERIFICATION", "Phone number verification failed");

            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);

                //Enviar OTP
                sendUserToProfileActivity(s);
            }
        };
    }

    public void setFields() {
        String text_age = getResources().getString(R.string.text_age);
        String ageLink = getResources().getString(R.string.ageLink);
        String text_age2 = getResources().getString(R.string.text_age2);
        String facebookBusinessLink = getResources().getString(R.string.facebookBusinessLink);

        text = "" + text_age +
                "<a href='https://www.whatsapp.com/legal/terms-of-service-eea?lg=es&lc=ES&eea=1#terms-of-service-age'> " + ageLink + ". </a> " +
                text_age2 +
                "<a href='https://faq.whatsapp.com/general/security-and-privacy/how-we-work-with-the-facebook-companies?lg=es&lc=ES&eea=1'> " +
                facebookBusinessLink + ".";

        noUnderlineSpan = new NoUnderlineSpan();
        Spannable s = noUnderlineSpan.noUnderlineSpan(text);
        registerConditions.setText(s);
        registerConditions.setMovementMethod(LinkMovementMethod.getInstance());


        textOk.setEnabled(!phone_number.getText().toString().isEmpty());

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadingBar.cancel();
    }

    public void createNewAccount(View view) {
        loadingBar.setTitle("Creando nueva cuenta");
        loadingBar.setMessage("Por favor espere, mientras estemos creando una nueva cuenta para ti..");
        loadingBar.setCanceledOnTouchOutside(true);
        loadingBar.show();

        generateOTP();
    }

    public void sendUserToProfileActivity(final String s) {

        Intent profileIntent = new Intent(RegisterActivity.this, ProfileActivity.class);
        profileIntent.putExtra("AuthCredentials", s);
        profileIntent.putExtra("phoneNumber", complete_phone_number);
        startActivity(profileIntent);

    }

    private boolean validatePhoneNumber(final String movil) {
        Pattern pattern = Pattern.compile("^[6|7][0-9]{8}$");
        return pattern.matcher(movil).matches();
    }


    private void generateOTP() {

        complete_phone_number = "+34" + phone_number.getText().toString();
        Log.e("asdd", " telefono a registrar: " + complete_phone_number);

        PhoneAuthOptions options = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber(complete_phone_number)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks(mCallbacks)
                .build();

        PhoneAuthProvider.verifyPhoneNumber(options);
    }


}