package cat.itb.whatsapp.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.Date;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.adapters.MensajeAdapterFirebase;
import cat.itb.whatsapp.firebase.FirebaseMensaje;
import cat.itb.whatsapp.firebase.FirestoreChat;
import cat.itb.whatsapp.adapters.FirestoreChatsAdapter;
import cat.itb.whatsapp.models.ChatModelFirestore;
import cat.itb.whatsapp.models.Mensaje;
import cat.itb.whatsapp.models.User;
import cat.itb.whatsapp.utils.ConvertSharePreferencesToUser;

public class MensajesChatActivity extends AppCompatActivity  implements View.OnClickListener{

    private RecyclerView recyclerView;
    public static User user;
    public String contactPhone;
    //public String contactName;


    // 1 FIREBASE ADAPTER //
    public static MensajeAdapterFirebase mensajeAdapterFirebase;
    // 1 FIRESTORE ADAPTER //
    //public static FirestoreMensajeAdapter firestoreMensajeAdapter;
    // 2 FIREBASE MODEL CHAT //
    //public static ChatModelFirebase chatModelFirebase;
    // 2 FIREBASE MODEL CHAT //
    public static ChatModelFirestore chatModelFirestore = new ChatModelFirestore();

    // TOP BAR //
    public ImageButton imageButtonGoToChatList, btnSentMessage;
    TextView textViewNameContact;
    // END TOP BAR //

    // BOTTOM BAR //
    public EditText editTextMessage;
    // END BOTTOM BAR //

    public boolean isNewChat= false;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_firebase);

        imageButtonGoToChatList = findViewById(R.id.btn_go_to_chat_list);
        btnSentMessage = findViewById(R.id.btn_send);

        imageButtonGoToChatList.setOnClickListener(v -> onBackPressed());


        getDataFromBundle();

        FirebaseMensaje.initializeDatabase(chatModelFirestore.getId());

        user = ConvertSharePreferencesToUser.getSharedPreferencesInfo(this);

        initializeRecyclerAndAdapter();

        contactPhone = getContactPhone();
        //contactName = getContactName();


        setViewChat();

        // SETEAMOS VALORES CONFORME SE HAN LEIDO LOS MENSAJES AL ENTRAR AL CHAT
        // SI EL CHAT ES NUEVO NO HACEMOS NADA
        if (!isNewChat) {
            updateIsReadChat();
        }

    }



    private void getDataFromBundle() {
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            switch ((int) bundle.get("CREATE")) {
                case 1:
                    chatModelFirestore = MainActivity.newChatInMain;
                    isNewChat = true;
                    break;

                default:
                    chatModelFirestore = FirestoreChatsAdapter.chatClickedInAdapter;
                    isNewChat = false;
                    break;
            }
        } else {
            Log.e("123", FirestoreChatsAdapter.chatClickedInAdapter.getId());
        }

    }


    private void initializeRecyclerAndAdapter() {
        recyclerView = findViewById(R.id.mensaje_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // FIREBASE //
        mensajeAdapterFirebase = new MensajeAdapterFirebase(FirebaseMensaje.firebaseRecyclerOptions);
        mensajeAdapterFirebase.setContext(getApplicationContext());
        recyclerView.setAdapter(mensajeAdapterFirebase);

    }

    private void setViewChat() {

        textViewNameContact = findViewById(R.id.text_view_name_contact);
        if (chatModelFirestore.getTelephones().size() == 2 ) {
            textViewNameContact.setText(contactPhone);

        } else {
            textViewNameContact.setText("Group");
        }

        editTextMessage = findViewById(R.id.edit_text_message);

        editTextMessage.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {//enter=66
                    String mensaje = editTextMessage.getText().toString();
                    if (!mensaje.equals("")) {
                        if (isNewChat) {
                            isNewChat = false;
                            insertMessageInNewChat(mensaje);
                        } else {
                            insertMessage(mensaje);
                        }

                        editTextMessage.setText("");
                    }
                }
                return true;
            }

        });

        btnSentMessage.setOnClickListener(v -> {
            String mensaje = editTextMessage.getText().toString();
            if (!mensaje.equals("")) {
                if (isNewChat) {
                    isNewChat = false;
                    insertMessageInNewChat(mensaje);
                } else {
                    insertMessage(mensaje);
                }

                editTextMessage.setText("");
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void updateIsReadChat() {

            chatModelFirestore.getIsReadMessagesHashMap().put(user.getTelefono(), true);

            chatModelFirestore.getConuntMessagesHashMap().put(user.getTelefono(), 0);

            FirestoreChat.updateChatByIsRead(chatModelFirestore);


    }

    public void insertMessage(String mensaje) {


        Mensaje m = new Mensaje("",
                chatModelFirestore.getId(),
                user.getTelefono(),
                contactPhone,
                new Date().toString(),
                mensaje);
        FirebaseMensaje.insert(m);

        chatModelFirestore.setMessage(m.getMensaje());
        chatModelFirestore.setMessageTime(new Date().toString());

        for (String contact: chatModelFirestore.getTelephones()) {

            if (!contact.equals(user.getTelefono())) {
                chatModelFirestore.getIsReadMessagesHashMap().put(contact, false);
                int countMessages = chatModelFirestore.getConuntMessagesHashMap().get(contact) +1;
                chatModelFirestore.getConuntMessagesHashMap().put(contact, countMessages);
            }
        }

        FirestoreChat.updateChatByNewMessage(chatModelFirestore);
    }

    public void insertMessageInNewChat(String mensaje) {

        String newId = FirestoreChat.getNewId();

        chatModelFirestore.setId(newId);
        chatModelFirestore.setMessage(mensaje);
        chatModelFirestore.setMessageTime(new Date().toString());

        // chat 2 personas
        if (chatModelFirestore.getTelephones().size()==2) {
            chatModelFirestore.getIsReadMessagesHashMap().put(contactPhone, false);
        } else { // grupo
            for (String phone: chatModelFirestore.getTelephones()){
                chatModelFirestore.getIsReadMessagesHashMap().put(phone, false);
                chatModelFirestore.getConuntMessagesHashMap().put(phone, 1);
            }
        }


        FirestoreChat.insert(chatModelFirestore);

        Mensaje m = new Mensaje("",
                newId,
                user.getTelefono(),
                contactPhone,
                new Date().toString(),
                mensaje);
        FirebaseMensaje.insert(m);

        FirebaseMensaje.initializeDatabase(chatModelFirestore.getId());
        initializeRecyclerAndAdapter();
        mensajeAdapterFirebase.startListening();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String getContactPhone() {
        String contactPhone = user.getTelefono();

        for (String phone: chatModelFirestore.getTelephones()) {
            if (!phone.equals(user.getTelefono()))
                contactPhone =  phone;
        }
        return  contactPhone;
    }



    @Override
    protected void onStart() {
        super.onStart();
        mensajeAdapterFirebase.startListening();
        //firestoreMensajeAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mensajeAdapterFirebase.stopListening();
        //firestoreMensajeAdapter.stopListening();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_go_to_chat_list:
                Intent intentToMainActivity = new Intent(this, MainActivity.class);

                if (!isNewChat) {
                    updateIsReadChat();
                } else {
                    intentToMainActivity.putExtra("FLAG_NO_CREATE_CHAT", 1);
                }

                this.startActivity(intentToMainActivity);

                break;

            default:
                break;

        }
    }
}


/* DEPRECATED
    public void insertMessageFirestore(String mensaje) {

        String emissor = user.getTelefono();
        String receptor ;
        if (emissor.equals(chatModelFirestore.getTelephones().get(0)))
            receptor = chatModelFirestore.getTelephones().get(1);
        else
            receptor = chatModelFirestore.getTelephones().get(0);

        FirestoreModeloMensaje m = new FirestoreModeloMensaje("",
                chatModelFirestore.getId(), //campo chat id del mensaje
                emissor,
                receptor,
                chatModelFirestore.getTelephones(),
                new Date().toString(),
                mensaje);

        FirestoreMensaje.insert(m);

        addMessageInChat(m);
    }

    public void addMessageInChat(FirestoreModeloMensaje mensaje) {
        chatModelFirestore.setMessage(mensaje.getMensaje());
        chatModelFirestore.setMessageTime(mensaje.getFecha());
        FirestoreChat.updateChatByNewMessage(chatModelFirestore);
    }




    //            Bundle bundle = getIntent().getExtras();
//
//            FirebaseMensaje.initializeDatabase((String) bundle.get("id"));
//
//            chatModelFirestore.setId((String) bundle.get("id"));
//            chatModelFirestore.setPhoto((String) bundle.get("photo"));
//            chatModelFirestore.setTelephones(
//                    new ArrayList<String>() {{
//                        add((String) bundle.get("telephone1"));
//                        add((String) bundle.get("telephone2"));
//                    }});
//
//
//            chatModelFirestore.setMessage((String) bundle.get("lastMessage"));
//            chatModelFirestore.setMessageTime((String) bundle.get("lastMessageTime"));
//            chatModelFirestore.setRead((boolean) bundle.get("isRead"));
* */
