package cat.itb.whatsapp.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.firebase.FirebaseAuthUser;
import cat.itb.whatsapp.models.User;
import cat.itb.whatsapp.utils.ImageUtils;

public class ProfileActivity extends AppCompatActivity {
    private MaterialTextView button_ok;
    private MaterialTextView edit_img;
    private TextInputEditText userName;
    private ShapeableImageView profilePicture;

    private TextInputEditText numberOtp;

    private String phoneNumber;
    private FirebaseAuthUser firebaseAuthUser;
    private User user = new User();


    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private String mAuthVerificationId;


    static final int REQUEST_IMAGE_CAPTURE = 100;
    File fileUrl;
    byte[] thumbByte = null;

    //Temp
    public static DatabaseReference users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Log.e("wwww", "phoneNumber");

        initializeFields();

        firebaseAuthUser = new FirebaseAuthUser();
        mAuth = firebaseAuthUser.getmAuth();
        currentUser = firebaseAuthUser.getCurrentUser();

        setFields();

    }

    public void initializeFields() {

        button_ok = findViewById(R.id.button_ok);
        userName = findViewById(R.id.user_name);
        edit_img = findViewById(R.id.edit_img);
        profilePicture = findViewById(R.id.profile_picture);
        numberOtp = findViewById(R.id.number_otp);


    }

    public void setFields() {


        setEnabledOK();

        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String optString = numberOtp.getText().toString();

                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mAuthVerificationId, optString);
                signInWithPhoneAuthCredential(credential);

            }
        });


        edit_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.startPickImageActivity(ProfileActivity.this);
            }
        });


        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                Log.d("TAG-NAME-LENGTH-B :", start + " - " + after);

                if (after == 0) {
                    userName.setError("required field");
                } else {
                    user.setName(s.toString());
                }

                setEnabledOK();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                Log.d("TAG-NAME-LENGTH-O :", count + "");

                int aux = s.toString().length();

                if (aux == 0) {
                    userName.setError("required field");
                } else {
                    user.setName(s.toString());
                }

                setEnabledOK();
            }

            @Override
            public void afterTextChanged(Editable s) {

                int aux = s.toString().length();
                Log.d("TAG-NAME-LENGTH-A :", aux + "");

                if (aux == 0) {
                    userName.setError("required field");
                } else {
                    user.setName(s.toString());
                }

                setEnabledOK();
            }
        });


        numberOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                Log.d("TAG-OPT-LENGTH-B :", start + " - " + after);

                if (after == 0) {
                    numberOtp.setError("required field");
                }
                if (after < 6) {
                    numberOtp.setError("invalid code");
                }

                setEnabledOK();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d("TAG-OPT-LENGTH-O :", count + "");

                int aux = s.toString().length();

                if (aux == 0) {
                    numberOtp.setError("required field");
                }
                if (aux < 6) {
                    numberOtp.setError("invalid code");
                }

                setEnabledOK();
            }

            @Override
            public void afterTextChanged(Editable s) {

                int aux = s.toString().length();
                Log.d("TAG-OTP-LENGTH-A :", aux + "");

                if (aux == 0) {
                    numberOtp.setError("required field");
                }
                if (aux < 6) {
                    numberOtp.setError("invalid code");
                }

                setEnabledOK();

            }
        });


        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            phoneNumber = bundle.getString("phoneNumber");
            Log.e("wwww", phoneNumber);
            mAuthVerificationId = bundle.getString("AuthCredentials");

            Log.d("TAG -PHONE_NUMBER", phoneNumber);
            Log.d("TAG -VERIFICATION_ID", mAuthVerificationId);

            users = firebaseAuthUser.getMyRefUsers();


            //Cargar datos si existe el usuario
            users.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    Log.d("TAG-onDataChange", phoneNumber);

                    if (snapshot.exists()) {

                        for (DataSnapshot userSnapshot : snapshot.getChildren()) {
                            User userAux = userSnapshot.getValue(User.class);

                            if (userAux.getTelefono().equals(phoneNumber)) {

                                Log.d("TAG - ID :", userAux.getId());
                                Log.d("TAG - INFO :", userAux.getInfo());
                                Log.d("TAG - NAME :", userAux.getName());
                                Log.d("TAG - TELEFONO :", userAux.getTelefono());
                                Log.d("TAG - URL PHOTO :", userAux.getUrlFoto());

                                user = userAux;

                                Picasso.with(getApplicationContext()).load(user.getUrlFoto()).into(profilePicture);
                                userName.setText(user.getName());

                                setEnabledOK();
                            }
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) { }
            });
        }
    }

    public void setEnabledOK() {

        if ((userName.getText().toString().length() != 0) && (numberOtp.getText().toString().length() != 0)) {
            Log.d("TAG-ENABLE-OK :", "TRUE");
            button_ok.setEnabled(true);
        } else {
            button_ok.setEnabled(false);
        }
    }


    public void sendUserToMainActivity() {

        thumbByte = ImageUtils.comprimirImagen(getApplicationContext(), fileUrl);

        //Temp
        user.setName(userName.getText().toString());
        user.setInfo("info user");

        firebaseAuthUser.insertUser(user, thumbByte);



        Intent mainIntent = new Intent(ProfileActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(ProfileActivity.this, new OnCompleteListener<AuthResult>() {

                    @SuppressLint("LongLogTag")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            user.setId(task.getResult().getUser().getUid());
                            user.setTelefono(task.getResult().getUser().getPhoneNumber());


                            //Setting values in Preference:
                            // MY_PREFS_NAME - a static String variable like:
                            String MY_PREFS_NAME = "MyPrefsFile";
                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putString("id", user.getId());
                            editor.putString("telefono", user.getTelefono());
                            editor.putString("urlFoto", user.getTelefono());
                            editor.putString("info", user.getInfo());
                            editor.putString("name", user.getName());



                            editor.apply();


                            sendUserToMainActivity();


                            //
                        } else {
                            // Sign in failed, display a message and update the UI
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid

                                /*otpFeedBAck.setVisibility(View.VISIBLE);
                                otpFeedBAck.setText("There was an error verifying OTP");*/
                            }
                        }

                        /*otpProgressBar.setVisibility(View.INVISIBLE);
                        verifyOtp.setEnabled(true);*/
                    }
                });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
            profilePicture.setImageBitmap(imageBitmap);
        }
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getApplicationContext(), data);
            ImageUtils.recortarImagen(imageUri, ProfileActivity.this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                fileUrl = new File(resultUri.getPath());
                Picasso.with(getApplicationContext()).load(fileUrl).into(profilePicture);

                //Añadir URL de la foto
                user.setUrlFoto(fileUrl.getAbsolutePath());
            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (currentUser != null) {
            sendUserToMainActivity();
        }
    }


}