package cat.itb.whatsapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import java.util.List;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.activities.MensajesChatActivity;

import cat.itb.whatsapp.models.Chat;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

    List<Chat> chatList;
    Context context;

    public ChatAdapter (List<Chat> chatList, Context context) {
        this.chatList = chatList;
        this.context = context;
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder {
        private ShapeableImageView imageViewShapableContact, shapeableImageViewIsRead;
        private TextView textViewName;
        private TextView textViewLastMessage;
        private TextView textViewTime;

        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewShapableContact = itemView.findViewById(R.id.imageViewContact);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewLastMessage = itemView.findViewById(R.id.textViewTipoLlamada);
            textViewTime = itemView.findViewById(R.id.textViewTimeLlamada);
            imageViewShapableContact = itemView.findViewById(R.id.imageViewContact);
            shapeableImageViewIsRead = itemView.findViewById(R.id.imageShapableIsIcInfo);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, MensajesChatActivity.class);
                    context.startActivity(intent);


                    /*NavDirections listToFragmentDirections = ChatListFragmentDirections.actionChatListFragmentToChatFragment(chatList.get(getAdapterPosition()));
                    Navigation.findNavController(v).navigate(listToFragmentDirections);*/

                }
            });
        }

        public void bind(Chat chat) {
            textViewName.setText(chat.getName());
            textViewLastMessage.setText(chat.getLastMessage());
            textViewTime.setText(chat.getLastMessageTime());
            /*imageViewShapableContact.setShapeAppearanceModel(imageViewShapableContact.getShapeAppearanceModel()
                    .toBuilder()
                    .setTopRightCorner(CornerFamily.ROUNDED,100f)
                    .build());*/
            Log.i("tag", " onClick"+ chat.isRead());



            //shapeableImageViewIsRead.setImageDrawable();

            shapeableImageViewIsRead.setVisibility(View.INVISIBLE);
            if (chat.isRead())
                shapeableImageViewIsRead.setVisibility(View.VISIBLE);


            /*shapeableImageViewIsRead.setShapeAppearanceModel(imageViewShapableContact.getShapeAppearanceModel()
                    .toBuilder()
                    .setTopRightCorner(CornerFamily.ROUNDED,100f)
                    .build());*/
        }
    }

    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_list, parent, false);
        return new ChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        Chat chat = chatList.get(position);
        holder.bind(chat);
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

}
