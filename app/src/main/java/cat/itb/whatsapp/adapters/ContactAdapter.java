package cat.itb.whatsapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.activities.MainActivity;
import cat.itb.whatsapp.models.Contact;
import cat.itb.whatsapp.models.ContactViewModel;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private List<Contact> contactList;
    private Context context;
    private boolean newGroup;
    //Button de cancelar nuevo grupo
    private ShapeableImageView cancelNewGroup;
    //Button de crear  grupo
    private ShapeableImageView done;
    private Intent intent;

    private final String KEY="Contacts";



    public ContactAdapter(List<Contact> contactList, Context context) {
        this.contactList = contactList;
        this.context = context;

        intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    }


    //Retornar los contactos selecionados
    @RequiresApi(api = Build.VERSION_CODES.N)
//    public static String[] getSelected(){
    public static String[] getSelected(){

        List<String> contacts= new ArrayList<>();

        ContactViewModel.getContacts().forEach((e)->{
            if (e.isGroup()){
                contacts.add(e.getNumber());
            }
        });



        String [] contactsToReturn = new String[contacts.size()];
        int i = 0;

        for(String number: contacts) {
            contactsToReturn[i] = number;
            i++;
        }

        return contactsToReturn;
//        return builder.toString().split(" ");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);


        //Cancelar nuevo grupo
        cancelNewGroup = parent.getRootView().findViewById(R.id.btn_cancel);
        //crear grupo
        done = parent.getRootView().findViewById(R.id.btn_createGroup);


        cancelNewGroup.setOnClickListener(view -> {

            cancelNewGroup.setVisibility(View.INVISIBLE);
            done.setVisibility(View.INVISIBLE);

            newGroup=false;
            setContactList(ContactViewModel.getContacts());
            Toast.makeText(context, "nuevo grupo cancelado" , Toast.LENGTH_SHORT).show();
            Toast.makeText(context, "CONTACTOS SELECCIONADOS: "+ContactViewModel.getContacts().stream().filter(Contact::isGroup).count(), Toast.LENGTH_SHORT).show();

            ContactViewModel.getContacts().forEach((e)->e.setGroup(false));
        });

        done.setOnClickListener(view -> {
            intent.putExtra(KEY, getSelected());

            context.startActivity(intent);
        });

        return new ContactViewHolder(v);
    }

    public void setContactList(List<Contact> contactList) {

        this.contactList = contactList;
        notifyDataSetChanged();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {

        Contact contact = contactList.get(position);
        holder.bind(contact);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private ShapeableImageView img;
        private CheckBox group;

        @SuppressLint("ResourceType")
        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            img = itemView.findViewById(R.id.img);
            group = itemView.findViewById(R.id.group);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Log.i("TAG-ITEM-CONTACT", "PHONE_NUMBER: " + ContactViewModel.getContacts().get(getAdapterPosition()).getNumber());

                    // si el item es nuevo grupo

                    if (name.getText().toString().equals("NUEVO GRUPO")){
                        setContactList(ContactViewModel.getContacts());
                        newGroup=true;
                        cancelNewGroup.setVisibility(View.VISIBLE);
                    }else {
                        newGroup=false;
                        if (getAdapterPosition()!=0 && getAdapterPosition()!=1){
                            //Anadir contact para enviar al MainActivity
                            intent.putExtra("newChatPhoneNumber", ContactViewModel.getContacts().get(getAdapterPosition()).getNumber());
                            context.startActivity(intent);
                        }
                    }
                }
            });
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        public void bind(Contact contact) {
            name.setText(contact.getName());
            img.setImageDrawable(AppCompatResources.getDrawable(context, contact.getImg()));
            if (newGroup && (getAdapterPosition()!=0 && getAdapterPosition()!=1)){
                group.setVisibility(View.VISIBLE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        contact.setGroup(!contact.isGroup());
                        //Toast.makeText(context, "aqui", Toast.LENGTH_SHORT).show();
                        group.setChecked(contact.isGroup());

                        //Comprobar si existe contactos seleccionados
                        if (isEmpty()){
                            done.setVisibility(View.INVISIBLE);
                            Toast.makeText(context, "vacio" ,Toast.LENGTH_SHORT).show();
                        }else{

                            if (!newGroup){

                                String phoneNumber=ContactViewModel.getContacts().get(getAdapterPosition()).getNumber();
                                Toast.makeText(context, "contacto selecionado: "+phoneNumber,Toast.LENGTH_SHORT).show();
                                intent.putExtra("newChatPhoneNumber", phoneNumber);
                                context.startActivity(intent);
                            }else {
                                Toast.makeText(context, "total selecionados: "+ContactViewModel.getContacts().stream().filter(Contact::isGroup).count(),Toast.LENGTH_SHORT).show();
                                done.setVisibility(View.VISIBLE);
                            }
                        }


                    }
                });

            }else {
                group.setVisibility(View.INVISIBLE);
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        public boolean isEmpty(){
            return ContactViewModel.getContacts().stream().noneMatch(Contact::isGroup);
        }
    }

}
