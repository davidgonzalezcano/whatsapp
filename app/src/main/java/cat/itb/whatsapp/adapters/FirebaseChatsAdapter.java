package cat.itb.whatsapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.imageview.ShapeableImageView;
import com.squareup.picasso.Picasso;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.activities.MensajesChatActivity;
import cat.itb.whatsapp.models.ChatModelFirebase;

public class FirebaseChatsAdapter extends FirebaseRecyclerAdapter
        <ChatModelFirebase, FirebaseChatsAdapter.ChatHolder> {

    public Context context;


    public FirebaseChatsAdapter(@NonNull FirebaseRecyclerOptions<ChatModelFirebase> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ChatHolder holder,
                                    int position, @NonNull ChatModelFirebase model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_chat_list, parent, false);
        return new ChatHolder(v);
    }


    public class ChatHolder extends RecyclerView.ViewHolder {

        private final ShapeableImageView shapeableImageViewIsRead;
        private final TextView textViewName;
        private final TextView textViewLastMessage;
        private TextView textViewTime;

        private ImageView imageViewContact;

        public ChatModelFirebase chat;

        public ChatHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewLastMessage = itemView.findViewById(R.id.textViewTipoLlamada);
            textViewTime = itemView.findViewById(R.id.textViewTimeLlamada);
            shapeableImageViewIsRead = itemView.findViewById(R.id.imageShapableIsIcInfo);
            imageViewContact = itemView.findViewById(R.id.imageViewContact);



            itemView.setOnClickListener(v -> {

                Intent intentToOneChat = new Intent(context, MensajesChatActivity.class);
                intentToOneChat.putExtra("CREATE", 0);
                intentToOneChat.putExtra("chat", chat);
                context.startActivity(intentToOneChat);


            });
        }

        public void bind(ChatModelFirebase chat) {

            this.chat = chat;

            textViewName.setText(chat.getTelephone2() );
            textViewLastMessage.setText(chat.getLastMessage());

            //String[] fechaCompleta = chat.getLastMessageTime().split(" ");
            ;
            textViewTime.setText(chat.getLastMessageTime());



            //CONTACT IMAGE

            try {
                Picasso.with(imageViewContact.getContext())
                        .load(chat.getPhoto())
                        .into(imageViewContact);
            } catch (Exception e) {
                Log.e("exception", e.toString());
            }

            //TICK IMAGE
            shapeableImageViewIsRead.setVisibility(View.INVISIBLE);
            if (!chat.isRead())
                shapeableImageViewIsRead.setVisibility(View.VISIBLE);

        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
