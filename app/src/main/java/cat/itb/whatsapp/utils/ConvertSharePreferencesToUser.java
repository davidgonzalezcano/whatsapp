package cat.itb.whatsapp.utils;

import android.app.Activity;
import android.content.SharedPreferences;

import cat.itb.whatsapp.models.User;

import static android.content.Context.MODE_PRIVATE;

public class ConvertSharePreferencesToUser {

    public static User getSharedPreferencesInfo(Activity activity) {
        String MY_PREFS_NAME = "MyPrefsFile";
        SharedPreferences prefs = activity.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String name = prefs.getString("name", "null");
        String telefono = prefs.getString("telefono", "null");
        String id = prefs.getString("id", "null");
        String urlFoto = prefs.getString("urlFoto", "null");
        String info = prefs.getString("info", "null");
        return new User(id, telefono, urlFoto, info, name);
    }
}
