package cat.itb.whatsapp.firebase;

import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.MutableLiveData;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cat.itb.whatsapp.models.User;

public class FirebaseAuthUser {
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    public static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference users;
    public static StorageReference storageReference;


    public FirebaseAuthUser() {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        Log.e("aaaaaa", "aaaaaa");

        firebaseDatabase = FirebaseDatabase.getInstance();
        users = firebaseDatabase.getReference("users");
        storageReference = FirebaseStorage.getInstance().getReference().child("users"); //carpeta del storage "users"

    }

    public DatabaseReference getMyRefUsers() {
        return users;
    }


    public String getUid() {
        return currentUser.getUid();
    }

    public FirebaseUser getCurrentUser() {
        return currentUser;
    }

    public FirebaseAuth getmAuth() {
        return mAuth;
    }


    public User findUserByPhoneNumber(final String phoneNumber) {

        /*users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if (snapshot.exists()) {
                    user = new ArrayList<>();

                    for (DataSnapshot userSnapshot : snapshot.getChildren()) {
                        User u = userSnapshot.getValue(User.class);

                        if (u.getTelefono().equals(phoneNumber)) {
                            Log.d("TAG - EXISTS :", "TRUE");
                            //
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
*/
        return null;


    }


    public void updateUser(User user) {
        users.child(user.getId()).setValue(user);
    }


    //guarda la imagen en storage, despues el user en database
    public void insertUser(final User user, byte[] thumbByte) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());
        String nameImg = timestamp + ".jpg";

        final StorageReference ref = storageReference.child(nameImg);
        StorageMetadata storageMetadata = new StorageMetadata.Builder()
                .setCustomMetadata("phone-number", user.getTelefono())
                .build();

        UploadTask uploadTask = ref.putBytes(thumbByte, storageMetadata);

        Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw (task.getException());
                }
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Uri downloadUri = task.getResult();
                user.setUrlFoto(downloadUri.toString());


                users.child(user.getId()).setValue(user);
            }
        });
    }


}
