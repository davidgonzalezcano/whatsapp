package cat.itb.whatsapp.firebase;

import android.util.Log;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import cat.itb.whatsapp.models.Mensaje;

public class FirebaseMensaje {
    public static final String PATH_NAME = "mensajes";
    public static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference databaseReference;
    public static FirebaseRecyclerOptions<Mensaje> firebaseRecyclerOptions;
    public static StorageReference storageReference;

    //SIN FILTRO
//    public static void inicializeDatabase(String telephone1, String telephone2) {
//        firebaseDatabase = FirebaseDatabase.getInstance();
//        databaseReference = firebaseDatabase.getReference("mensajes");
//        firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Mensaje>()
//                .setQuery(databaseReference, Mensaje.class).build();
//
//    }
    public static void initializeDatabase(String chatId) {
        firebaseDatabase = FirebaseDatabase.getInstance();
        //TODO: cambiar la query para que coja todos los mensajes con
        Log.e("asdd", "chat id para buscar los mensajes: " + chatId);
        Query query = firebaseDatabase
                .getReference(PATH_NAME)
                .orderByChild("chatId").equalTo(chatId);

        firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Mensaje>()
                .setQuery(query,  Mensaje.class).build();

        //importante, sino no tenemos la base de datos mensajes!!!!!!!!
        databaseReference = firebaseDatabase.getReference("mensajes");
    }


    public static void insert(Mensaje mensaje) {
        String key = databaseReference.push().getKey();
        assert key != null;
        mensaje.setId(key);
        databaseReference.child(key).setValue(mensaje);

    }

    public static void delete(String id) {
        databaseReference.child(id).removeValue();
    }

    public static void deleteAll(String id) {

        Query applesQuery = databaseReference.orderByChild("chatId").equalTo(id);

        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("TAG", "onCancelled", databaseError.toException());
            }
        });

    }

}
