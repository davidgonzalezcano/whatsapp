package cat.itb.whatsapp.firebase;


import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Date;

import cat.itb.whatsapp.models.User;

public class FirebaseUser {

    public static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference databaseReference;
    public static FirebaseRecyclerOptions<User> firebaseRecyclerOptions;
    public static StorageReference storageReference;



    public FirebaseUser() { }


    public static void insert(User u) {
        String key = databaseReference.push().getKey();
        assert key != null;
        u.setId(key);
        databaseReference.child(key).setValue(u);
    }

    public void delete(User u) {
        databaseReference.child(u.getId()).removeValue();
    }

    //guarda la imagen en storage, despues en database
    public static void saveUser(final User u, byte[] thumbByte) {

        inicializeDatabase();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());
        String nameImg = timestamp + ".jpg";

        final StorageReference ref = storageReference.child(nameImg);
        StorageMetadata storageMetadata = new StorageMetadata.Builder()
                .setCustomMetadata("phone", u.getTelefono())
                .build();

        UploadTask uploadTask = ref.putBytes(thumbByte, storageMetadata);

        Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()){
                    throw (task.getException());
                }
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Uri downloadUri = task.getResult();
                u.setUrlFoto(downloadUri.toString());
                String key = databaseReference.push().getKey();
                assert key != null;
                u.setId(key);
                databaseReference.child(key).setValue(u);
            }
        });
    }

    public static void inicializeDatabase() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("users");
        firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<User>()
                .setQuery(databaseReference, User.class).build();
        storageReference = FirebaseStorage.getInstance().getReference().child("users"); //carpeta del storage "users"
    }


    //TODO RETORNAR EL USUARIO QUE COINCIDA CON EL TELEFONO
    public User getUserByPhone(String phone) {
        User user = new User();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    User u = postSnapshot.getValue(User.class);
                    Log.e("user firebase",u.getName());

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        return user;
    }

}
