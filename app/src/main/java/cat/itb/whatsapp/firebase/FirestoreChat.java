package cat.itb.whatsapp.firebase;
import android.util.Log;

import androidx.annotation.NonNull;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.SetOptions;

import cat.itb.whatsapp.firebase.FirebaseMensaje;
import cat.itb.whatsapp.models.ChatModelFirestore;

public class FirestoreChat {
    public static final String COLLECTION_NAME = "chatsfirestore";

    public static FirebaseFirestore db ;
    public static DatabaseReference databaseReference;
    public static FirestoreRecyclerOptions<ChatModelFirestore> firebaseFirestoreRecyclerOptions;

    public static void inicializeDatabase(String param) {
        db= FirebaseFirestore.getInstance();
        Query query = db.collection(COLLECTION_NAME) //;
                //.whereIn("telephone1", Collections.singletonList(param))
                .whereArrayContains("telephones", param);

//                .whereIn("telephone2", Collections.singletonList(param));


        firebaseFirestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<ChatModelFirestore>()
                .setQuery(query, ChatModelFirestore.class)
                .build();


    }

    // TODO inserta y actualiza
    public static void insert(ChatModelFirestore chatModelFirestore) {
        Log.e("1111", "insertamos el chat con id " + chatModelFirestore.getId());
        if (chatModelFirestore.getId() == null ) {
            DocumentReference doc =  db.collection(COLLECTION_NAME).document();
            chatModelFirestore.setId(doc.getId());
            doc.set(chatModelFirestore, SetOptions.merge());
        } else {
            DocumentReference doc =  db.collection(COLLECTION_NAME).document(chatModelFirestore.getId());
            doc.set(chatModelFirestore, SetOptions.merge());
        }
    }

    public static void insertChatInMainActivity(ChatModelFirestore chatModelFirestore) {
        DocumentReference doc =  db.collection(COLLECTION_NAME).document();
        chatModelFirestore.setId(doc.getId());
        doc.set(chatModelFirestore, SetOptions.merge());
    }

    // TODO
    public static void updateChatByNewMessage(ChatModelFirestore chatModelFirestore) {
        DocumentReference doc =  db.collection(COLLECTION_NAME).document(chatModelFirestore.getId());
        Log.d("firestorechat", chatModelFirestore.getId());
//        doc.update("lastMessage", chatModelFirestore.getLastMessage()).addOnSuccessListener(aVoid ->
//                Log.d("TAG", "DocumentSnapshot successfully updated!"))
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.w("TAG", "Error updating document", e);
//                    }
//                });


        doc.set(chatModelFirestore, SetOptions.merge());
        //doc.update("lastMessageTime", chatModelFirestore.getMessageTime());

    }



    public static String getNewId() {
        DocumentReference doc =  db.collection(COLLECTION_NAME).document();
        return doc.getId();
    }

    public static void updateChatByIsRead(ChatModelFirestore chatModelFirestore) {
        DocumentReference doc =  db.collection(COLLECTION_NAME).document(chatModelFirestore.getId());
        doc.set(chatModelFirestore);
    }

    public static void delete(String id) {
        db.collection(COLLECTION_NAME).document(id).delete();
        FirebaseMensaje.initializeDatabase(id);
        FirebaseMensaje.deleteAll(id);
    }
}
