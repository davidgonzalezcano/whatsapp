package cat.itb.whatsapp.firestore;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.SetOptions;

import cat.itb.whatsapp.models.FirestoreModeloMensaje;

public class FirestoreMensaje {
    public static final String COLLECTION_NAME = "mensajesfirestore";

    public static FirebaseFirestore db ;
    public static DatabaseReference databaseReference;
    public static FirestoreRecyclerOptions<FirestoreModeloMensaje> firebaseFirestoreRecyclerOptions;

    public static void inicializeDatabase(String param) {
        db= FirebaseFirestore.getInstance();
        Query query = db.collection(COLLECTION_NAME) //;
                .whereArrayContains("telephones", param);
                //.orderBy("fecha", Query.Direction.DESCENDING)



        firebaseFirestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<FirestoreModeloMensaje>()
                .setQuery(query, FirestoreModeloMensaje.class)
                .build();
    }

    public static void insert(FirestoreModeloMensaje firestoreModeloMensaje) {
        DocumentReference doc =  db.collection(COLLECTION_NAME).document();
        firestoreModeloMensaje.setId(doc.getId());
        doc.set(firestoreModeloMensaje, SetOptions.merge());
    }




}
