package cat.itb.whatsapp.models;

import java.io.Serializable;

public class User implements Serializable {

    private String id;
    private String telefono;
    private String urlFoto;
    private String info;
    private String name;

    public User(String id, String telefono, String urlFoto, String info, String name) {
        this.id = id;
        this.telefono = telefono;
        this.urlFoto = urlFoto;
        this.info = info;
        this.name = name;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
