package cat.itb.whatsapp.models;

import java.io.Serializable;

public class ChatModelFirebase implements Serializable {
    private String id;
    private String photo;
    private String telephone1;
    private String telephone2;
    private String lastMessage;
    private String lastMessageTime;
    private boolean isRead;

    public ChatModelFirebase() { }

    public ChatModelFirebase(String id, String photo, String telephone1, String telephone2, String lastMessage, String lastMessageTime, boolean isRead) {
        this.id = id;
        this.photo = photo;
        this.telephone1 = telephone1;
        this.telephone2 = telephone2;
        this.lastMessage = lastMessage;
        this.lastMessageTime = lastMessageTime;
        this.isRead = isRead;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTelephone1() {
        return telephone1;
    }

    public void setTelephone1(String telephone1) {
        this.telephone1 = telephone1;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

}
