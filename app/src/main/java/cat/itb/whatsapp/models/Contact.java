package cat.itb.whatsapp.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import cat.itb.whatsapp.R;

public class Contact implements Parcelable {
    private String name;
    private String number;
    private int img;
    private boolean group;



    public Contact(String name, String number) {
        this.name = name;
        this.number = number;
        this.img = R.drawable.contact;
        this.group=false;
    }


    public Contact(String name, String number, int img) {
        this.name = name;
        this.number = number;
        this.img = img;
        this.group=false;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    protected Contact(Parcel in) {
        name = in.readString();
        number = in.readString();
        img = in.readInt();
        group = in.readBoolean();
    }


    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(number);
        dest.writeInt(img);
        dest.writeBoolean(group);

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
