package cat.itb.whatsapp.models;

import java.io.Serializable;
import java.util.ArrayList;

public class FirestoreModeloMensaje implements Serializable {
    String id;
    String chatId;
    String telefonoEmisor;
    String telefonoReceptor;
    private ArrayList<String> telephones;
    String fecha;
    String mensaje;

    public FirestoreModeloMensaje() { }

    public FirestoreModeloMensaje(String id,
                                  String chatId,
                                  String telefonoEmisor,
                                  String telefonoReceptor,
                                  ArrayList<String> telephones,
                                  String fecha,
                                  String mensaje) {
        this.id = id;
        this.chatId = chatId;
        this.telefonoEmisor = telefonoEmisor;
        this.telefonoReceptor = telefonoReceptor;
        this.telephones = telephones;
        this.fecha = fecha;
        this.mensaje = mensaje;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getChatId() { return chatId; }

    public void setChatId(String chatId) { this.chatId = chatId; }

    public String getTelefonoEmisor() { return telefonoEmisor; }

    public void setTelefonoEmisor(String telefonoEmisor) { this.telefonoEmisor = telefonoEmisor;}

    public String getTelefonoReceptor() { return telefonoReceptor; }

    public void setTelefonoReceptor(String telefonoReceptor) { this.telefonoReceptor = telefonoReceptor; }

    public ArrayList<String> getTelephones() { return telephones; }

    public void setTelephones(ArrayList<String> telephones) { this.telephones = telephones; }

    public String getFecha() { return fecha; }

    public void setFecha(String fecha) { this.fecha = fecha; }

    public String getMensaje() { return mensaje; }

    public void setMensaje(String mensaje) { this.mensaje = mensaje; }
}
