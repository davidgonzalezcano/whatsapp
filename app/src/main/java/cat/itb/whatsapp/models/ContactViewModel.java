//    private String[] names = {"NUEVO GRUPO", "NUEVO CONTACTO", "Kenneth", "David", "Jhosmar", "Phantom contact"};
//    private String[] phoneNumbers = {"", "", "+34650190003", "+34670815219", "+34623456789", "+34666666666"};
//    private int[] images = {R.drawable.new_group, R.drawable.contact_add, R.drawable.contact, R.drawable.contact, R.drawable.contact, R.drawable.contact};

package cat.itb.whatsapp.models;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import cat.itb.whatsapp.R;

public class ContactViewModel extends ViewModel {

    public static List<Contact> contacts = null;

    private String[] names = {"NUEVO GRUPO", "LISTA CONTACTOS"};
    private String[] phoneNumbers = {"", ""};
    private int[] images = {R.drawable.new_group, R.drawable.contact_add};


    public ContactViewModel() {

        contacts = new ArrayList<>();

        for (int i = 0; i < names.length; i++) {
            Contact contact = new Contact(names[i], phoneNumbers[i], images[i]);

            contacts.add(contact);
        }
    }

    public void add(Contact contact){
        contacts.add(contact);
    }

    public static List<Contact> getContacts() {
        return contacts;
    }

}

