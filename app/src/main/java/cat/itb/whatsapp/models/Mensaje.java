package cat.itb.whatsapp.models;

import java.io.Serializable;

public class Mensaje implements Serializable {

    String id;
    String chatId;
    String telefonoEmisor;
    String telefonoReceptor;
    String fecha;
    String mensaje;

    public Mensaje(String id, String chatId, String telefonoEmisor, String telefonoReceptor, String fecha, String mensaje) {
        this.id = id;
        this.chatId = chatId;
        this.telefonoEmisor = telefonoEmisor;
        this.telefonoReceptor = telefonoReceptor;
        this.fecha = fecha;
        this.mensaje = mensaje;
    }
    public Mensaje(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getTelefonoEmisor() {
        return telefonoEmisor;
    }

    public void setTelefonoEmisor(String telefonoEmisor) {
        this.telefonoEmisor = telefonoEmisor;
    }

    public String getTelefonoReceptor() {
        return telefonoReceptor;
    }

    public void setTelefonoReceptor(String telefonoReceptor) {
        this.telefonoReceptor = telefonoReceptor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
