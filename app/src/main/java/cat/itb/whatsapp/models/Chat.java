package cat.itb.whatsapp.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

public class Chat implements Parcelable {

    private String chatId;
    private String photo;
    private String name;
    private String lastMessage;
    private String lastMessageTime;
    private boolean isRead;

    public Chat(String chatId, String photo, String name, String lastMessage, String lastMessageTime, boolean isRead) {
        this.chatId = chatId;
        this.photo = photo;
        this.name = name;
        this.lastMessage = lastMessage;
        this.lastMessageTime = lastMessageTime;
        this.isRead = isRead;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    protected Chat(Parcel in) {
        chatId = in.readString();
        photo = in.readString();
        name = in.readString();
        lastMessage = in.readString();
        lastMessageTime = in.readString();
        isRead= in.readBoolean();
    }

    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public Chat createFromParcel(Parcel in) {
            return new Chat(in);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(chatId);
        dest.writeString(photo);
        dest.writeString(name);
        dest.writeString(lastMessage);
        dest.writeString(lastMessageTime);
        dest.writeBoolean(isRead);
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
